export default class CardService{

    restClient;

    constructor(restClient){
        this.restClient = restClient;
    }

    async loadCards(filter){
       return await this.restClient.post("/search", filter);
    }

    async loadById(id){
        return await this.restClient.get("/card/"+id);
    }

    async save(card) {
        if(!card.strength){
            card.strength = -1;
        }
        await this.restClient.put("/card/"+card.id, card)
    }
}