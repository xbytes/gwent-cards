import {AppNavigatorBase} from "../apputil/apputil.js"

export class AppNavigator extends AppNavigatorBase{
    details = (id) => this.navigate("details?id="+id);
    isDetails = () => this.isPage("details")
}
export const appNavigator = new AppNavigator();