import CardService from "./card/CardService";
import {RestClient} from "../apputil/apputil.js";
import {appNavigator, AppNavigator} from "./AppNavigator.js";

export default class AppBackend{
    cardService: CardService;

    constructor(personService:CardService){
        this.cardService = personService;
    }

    public static async create(url:string){
        let restClient = await RestClient.create(url)
        let cardService = new CardService(restClient);
        return new AppBackend(cardService);
    }
}

export var appBackend:AppBackend = null;
AppBackend.create("http://localhost:8080").then((a)=> {
    appBackend = a;
    appNavigator.home();
});