import {AppElement, AppState, ShadowElement} from "./apputil/apputil.js";



class PopUpInfo extends ShadowElement {
    connectedCallback(){
        this.render(`
            hello <b>${this.attr('myattr')}</b>
            <p></p>
        `);
        let p = this.el('p');
        p.innerHTML='gutem morgen';
    }
}

customElements.define('popup-info', PopUpInfo);

class MyButton extends AppElement{
    connectedCallback(){
        this.render(`
                <button>${this.attr('text')}</button>
        `);
    }

    static get observedAttributes() {return ['text']; }
    attributeChangedCallback(name, oldValue, newValue) {
        this.connectedCallback();
    }
}

customElements.define('my-button', MyButton);
