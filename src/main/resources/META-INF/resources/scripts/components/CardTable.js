import {AppElement, NotEmpty, RestClient, state} from "../apputil/apputil.js";
import CardService from "../app/card/CardService.js";

class CardTable extends AppElement{
    filter = {owned:'', ability:'', region:'', location:'', player:'', questName:''}

    connectedCallback() {
        this.load();
    }

    set CardList(value) {
        this.cardList = value;
        this.load();
    }

    async filterChanged(t){
        t.el('tbody').remove();
        t.el('table').append(await this.loadBody());

    }

    async load(){
        let table = this.create('table', {'border':'1'});
            let head = this.create('thead');
                let row = this.create('tr');
                    let cell1 = this.create('th');
                        let select = this.create('select');
                        select.append(this.create('option', {'value':'', 'selected':'selected'}, 'Both'));
                        select.append(this.create('option', {'value':'true'}, 'Owned'));
                        select.append(this.create('option', {'value':'false'}, 'Not Owned'));
                        select.addEventListener('change', (e)=> {this.filter.owned = e.target.value; this.filterChanged(this)});
                    cell1.append(select);
                row.append(cell1)
                    let cell2 = this.create('th');
                        let input = this.create('input', {'placeholder':'name'});
                        input.addEventListener('keyup', (e)=> {this.filter.name = e.target.value; this.filterChanged(this)});
                    cell2.append(input);
                row.append(cell2)
                    let cell3 = this.create('th');
                        let select2 = this.create('select');
                        select2.append(this.create('option', {'value':'','selected':'selected'}, 'All fractions'));
                        select2.append(this.create('option', {}, 'Monsters'));
                        select2.append(this.create('option', {}, 'Nilfgaardian Empire'));
                        select2.append(this.create('option', {}, 'Northern Realms'));
                        select2.append(this.create('option', {}, 'Scoia\'tael'));
                        select2.append(this.create('option', {}, 'Skellige'));
                        select2.append(this.create('option', {}, 'Neutral cards'));
                        select2.addEventListener('change', (e)=> {this.filter.fraction = e.target.value; this.filterChanged(this)});
                cell3.append(select2);
              row.append(cell3)
                let cell5 = this.create('th');
                let select5 = this.create('select');
                select5.append(this.create('option', {'value':'','selected':'selected'}, 'All game parts'));
                select5.append(this.create('option', {}, 'Base Game'));
                select5.append(this.create('option', {}, 'Hearts of Stone'));
                select5.append(this.create('option', {}, 'Blood and Wine'));
                select5.addEventListener('change', (e)=> {this.filter.gamePart = e.target.value; this.filterChanged(this)});
                cell5.append(select5);
              row.append(cell5);
              let select6 = this.create('select', {}, [
                  this.create('option', {'selected':'selected', 'value':''}, 'All types'),
                  this.create('option', {}, 'Unit'),
                  this.create('option', {}, 'Hero'),
                  this.create('option', {}, 'Leader'),
                  this.create('option', {}, 'Special'),
                  this.create('option', {}, 'Weather')
              ]);
              select6.addEventListener('change', (e)=> {this.filter.type = e.target.value; this.filterChanged(this)});
              row.append(this.create('th',{},select6))
                let select7 = this.create('select', {}, [
                    this.create('option', {'selected':'selected', 'value':''}, 'All rows'),
                    this.create('option', {}, 'agile'),
                    this.create('option', {}, 'melee'),
                    this.create('option', {}, 'range'),
                    this.create('option', {}, 'siege'),
                    this.create('option', {'value':null}, 'any')
                ]);
                select7.addEventListener('change', (e)=> {this.filter.row = e.target.value; this.filterChanged(this)});
                row.append(this.create('th',{}, select7));
                row.append(this.create('th',{},'strength'));
                row.append(this.create('th',{}, this.createInput('ability', this.filter.ability, (value) => {this.filter.ability = value;this.filterChanged(this);})));
                row.append(this.create('th',{}, this.createInput('region', this.filter.region, (value) => {this.filter.region = value;this.filterChanged(this);})));
                row.append(this.create('th',{}, this.createInput('location', this.filter.location, (value) => {this.filter.location = value;this.filterChanged(this);})));
                row.append(this.create('th',{}, this.createInput('player', this.filter.player, (value) => {this.filter.player = value;this.filterChanged(this);})));
                row.append(this.create('th',{}, this.createInput('quest name', this.filter.questName, (value) => {this.filter.questName = value;this.filterChanged(this);})));

        head.append(row);
        table.append(head);
        table.append(await this.loadBody());
        this.append(table);
    }

    async loadBody() {
        let res = await new CardService(await RestClient.create(this.attr('baseurl'))).loadCards(this.filter);
        let tbody = this.create('tbody');
        for (let i = 0; i < res.length; i++) {
            let card = NotEmpty.of(res[i]);
            let tr = this.create('tr');
            tr.append(this.create('td', {}, card.load('owned')));
            tr.append(this.create('td', {}, card.load('cardName')));
            tr.append(this.create('td', {}, card.load('fraction')));
            tr.append(this.create('td', {}, card.load('gamePart')));

            tr.append(this.create('td', {}, card.load('type')));
            tr.append(this.create('td', {}, card.load('row')));
            tr.append(this.create('td', {}, card.load('strength', true)));
            tr.append(this.create('td', {}, card.load('ability')));

            tr.append(this.create('td', {}, card.load('region')));
            tr.append(this.create('td', {}, card.load('location')));
            tr.append(this.create('td', {}, card.load('player')));
            tr.append(this.create('td', {}, card.load('questName')));
            tbody.append(tr);
        }
        return tbody;
    }
}

customElements.define('app-cardtable', CardTable);