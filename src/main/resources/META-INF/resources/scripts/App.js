import {AppElement, NotEmpty, RestClient, state} from "./apputil/apputil.js";
import "./components/CardTable.js";

export class App extends AppElement{
    connectedCallback() {
        this.load();
    }

    async load(){
       this.render(`<app-cardtable baseurl="${this.attr('src')}"/>`);

       this.el('app-cardtable');
    }
}

customElements.define('app-main', App);