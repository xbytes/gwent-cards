package org.acme.card.boundary;

import io.quarkus.logging.Log;
import org.acme.card.entity.Card;
import org.acme.parser.CardLoader;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Transactional
@RequestScoped
public class CardService {
    @Inject
    EntityManager em;

    @Inject
    CardLoader cl;

    public List<Card> loadAll() throws IOException {
        List<Card> all = em.createNamedQuery(Card.ALL, Card.class).getResultList();
        loadIfEmpty(all);

        return all;
    }

    private void loadIfEmpty(List<Card> all) throws IOException {
        if(all.isEmpty()){
            var li = cl.loadAllRemoteCards();
            Log.info("Cards loaded from resource.");
            li.forEach(e -> em.merge(e));
            all.add(new Card());
            all.get(0).cardName = "Cards generated. ReloadPage";
        }
    }

    public List<Card> search(FilterValues filter) throws IOException {
        var matches = new ArrayList<Card>();
        List<Card> allCards = loadAll();
        loadIfEmpty(allCards);
        for (var card: allCards) {
            boolean showCard = true;
            if(!(filter.owned == null || filter.owned.isEmpty())){
                if(!filter.owned.equals(String.valueOf(card.owned))){
                    continue;
                }
            }
            if(!(filter.name == null || filter.name.isEmpty())){
                if(!card.cardName.toLowerCase().contains(filter.name.toLowerCase())){
                    continue;
                }
            }
            if(!(filter.fraction == null || filter.fraction.isEmpty())){
                if(!card.fraction.toLowerCase().contains(filter.fraction.toLowerCase())){
                    continue;
                }
            }
            if(!(filter.gamePart == null || filter.gamePart.isEmpty())){
                if(!card.gamePart.toLowerCase().contains(filter.gamePart.toLowerCase())){
                    continue;
                }
            }
            if(!(filter.type == null || filter.type.isEmpty())){
                if(!card.type.toLowerCase().contains(filter.type.toLowerCase())){
                    continue;
                }
            }
            if(!(filter.row == null || filter.row.isEmpty())){
                if(!card.row.toLowerCase().contains(filter.row.toLowerCase())){
                    continue;
                }
            }
            if(!(filter.ability == null || filter.ability.isEmpty())){
                if(!card.ability.toLowerCase().contains(filter.ability.toLowerCase())){
                    continue;
                }
            }
            if(!(filter.region == null || filter.region.isEmpty())){
                if(!card.region.toLowerCase().contains(filter.region.toLowerCase())){
                    continue;
                }
            }
            if(!(filter.location == null || filter.location.isEmpty())){
                if(!card.location.toLowerCase().contains(filter.location.toLowerCase())){
                    continue;
                }
            }
            if(!(filter.player == null || filter.player.isEmpty())){
                if(!card.player.toLowerCase().contains(filter.player.toLowerCase())){
                    continue;
                }
            }
            if(!(filter.questName == null || filter.questName.isEmpty())){
                if(!card.questName.toLowerCase().contains(filter.questName.toLowerCase())){
                    continue;
                }
            }


            matches.add(card);
        }

        return matches;
    }

    public Card findById(Long id) {
        return em.find(Card.class, id);
    }

    public Card save(Card card) {
        return em.merge(card);
    }
}
