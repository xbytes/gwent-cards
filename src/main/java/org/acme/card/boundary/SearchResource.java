package org.acme.card.boundary;

import org.acme.card.entity.Card;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("search")
public class SearchResource {

    @Inject
    CardService cardService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Card> search(FilterValues filter) throws IOException {
        return cardService.search(filter);
    }
}
