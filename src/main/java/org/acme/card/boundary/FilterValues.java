package org.acme.card.boundary;

import javax.persistence.Column;
import java.util.Objects;

public class FilterValues {
    public String owned;

    public String name;
    public String fraction;
    public String gamePart;

    public String type;
    public String row;
    public String ability;

    public String region;
    public String location;
    public String player;
    public String questName;
}
