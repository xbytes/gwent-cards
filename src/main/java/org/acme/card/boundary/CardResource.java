package org.acme.card.boundary;

import org.acme.card.entity.Card;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("card")
public class CardResource {

    @Inject
    CardService cardService;

    @GET
    public List<Card> listAll() throws IOException {
        return cardService.loadAll();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Card byId(@PathParam("id") Long id){
        return cardService.findById(id);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Card save(@PathParam("id") Long id, Card card){
        card.id = id;
        if(card.strength.compareTo(-1) == 0){
            card.strength = null;
        }
        return cardService.save(card);
    }

}
