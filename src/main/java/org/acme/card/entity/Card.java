package org.acme.card.entity;

import javax.persistence.*;

@Entity
@NamedQuery(name = Card.ALL, query = "SELECT c FROM Card c")
public class Card {
    public static final String ALL = "all";
    @Id
    @GeneratedValue
    public Long id;
    public boolean owned;
    public byte[] image;

    public String cardName;
    public String fraction;
    public String gamePart;

    public String type;
    @Column(length = 2000)
    public String row;
    public Integer strength;
    public String ability;

    public String region;
    @Column(length = 2000)
    public String location;
    public String player;
    public String questName;
}
