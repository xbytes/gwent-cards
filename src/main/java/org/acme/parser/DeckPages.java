package org.acme.parser;

enum DeckPages {
    MONSTERS(           "Monsters",           "https://witcher.fandom.com/wiki/Monsters_Gwent_deck"),
    NILFGAARDIAN_EMPIRE("Nilfgaardian Empire","https://witcher.fandom.com/wiki/Nilfgaardian_Empire_Gwent_deck"),
    NORTHERN_REALMS(    "Northern Realms",    "https://witcher.fandom.com/wiki/Northern_Realms_Gwent_deck"),
    SCOIATAEL(          "Scoia'tael",         "https://witcher.fandom.com/wiki/Scoia%27tael_Gwent_deck"),
    SKELLIGE(           "Skellige",           "https://witcher.fandom.com/wiki/Skellige_Gwent_deck"),
    NEUTRAL_CARDS(      "Neutral cards",      "https://witcher.fandom.com/wiki/Gwent_neutral_cards");
    
    public final String fractionName;
    public final String url;
    DeckPages(String fractionName, String url) {
        this.fractionName = fractionName;
        this.url = url;
    }
}
