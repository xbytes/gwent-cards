package org.acme.parser;

record FandomDeckOverviewRow(
        byte[] icon,
        String name,
        String type,
        String row,
        String strength,
        String ability,
        String source
) {}
