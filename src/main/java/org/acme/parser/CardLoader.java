package org.acme.parser;

import org.acme.card.entity.Card;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Dependent
public class CardLoader {

    @Inject
    CardMapper cardMapper;

    public List<Card> loadAllRemoteCards() throws IOException {
        List<Card> allCards = new ArrayList<>();
        for (DeckPages deckPage: DeckPages.values()){
            allCards.addAll(loadCardsFrom(deckPage));
            //break; // only one during testing
        }
        return allCards;
     }

    private List<Card> loadCardsFrom(DeckPages deckPageEnum) throws IOException {
        Document doc = Jsoup.connect(deckPageEnum.url).get();
        Element table = doc.select("table").get(0); //select the first table.
        Elements rows = table.select("tr");

        return rowsToCardList(rows, deckPageEnum);
    }

    private List<Card> rowsToCardList(Elements rows, DeckPages deckPage) {
        List<Card> tableCards = new ArrayList<>();
        for (int i = 1; i < rows.size(); i++) { //first row is the col names so skip it.
            Element row = rows.get(i);
            Elements cols = row.select("td");
            var overviewTableRow = new FandomDeckOverviewRow(
                    null,
                    cols.get(1).text(),
                    cols.get(2).text(),
                    cols.select("img").first().attr("alt"),
                    cols.get(4).text(),
                    cols.get(5).text(),
                    cols.get(6).text()
            );
            List<Card> mappedCard = cardMapper.mapRowToCards(overviewTableRow, deckPage.fractionName);
            tableCards.addAll(mappedCard);
            //return tableCards; // Only one during development to prevent visiting all pages
        }

        return tableCards;
    }
}
