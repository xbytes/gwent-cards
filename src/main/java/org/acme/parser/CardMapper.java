package org.acme.parser;

import org.acme.card.entity.Card;

import javax.enterprise.context.Dependent;
import java.util.ArrayList;
import java.util.List;

@Dependent
class CardMapper {

    public List<Card> mapRowToCards(FandomDeckOverviewRow row, String fractionName) {
        List<Card> cards = new ArrayList<>();
        if(row.source() != null && row.source().contains("1 purchased")){
            String[] locations = row.source().split("1 purchased");
            int numberOfOccurrence = locations.length;
            for (int i = 1; i < numberOfOccurrence; i++) {
                cards.add(createCard(row, fractionName, locations[i].trim()));
            }
        }else {
            cards.add(createCard(row, fractionName, row.source()));
        }

        return cards;
    }

    private Card createCard(FandomDeckOverviewRow row, String fractionName, String location) {
        Card card = new Card();
        card.cardName = row.name();
        card.fraction = fractionName;
        card.gamePart = "";

        card.type = row.type();
        card.row = row.row();
        String str = row.strength();
        card.strength = str.isEmpty() ? null : Integer.parseInt(str);
        card.ability = row.ability();

        card.region = "";
        card.location = location;
        card.player = "";
        card.questName = "";
        return card;
    }
}
